//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var Path = require( 'path' )
  , Promise = require( 'bluebird' )
  , fs = require( 'fs' )
  , bodyParser = require( 'body-parser' )
  , net = require( 'net' )
  , Datastore = require( 'nedb' )
  , SocketConnection = require( './socket-connection' )

var Application = function( rootDir ) {
    var self = this;
    self.connections = []
    self.express = require( 'express' )()
}

//
// #############################################################################
//

Application.prototype.start = function( http_port, socket_port, api ) {
    var self = this;
    self.status = {
        status: 'starting',
        api_port: http_port,
        push_port: socket_port,
        api: api,
        active_players: 0
    }

    self.config = {
        generator: require( './config/generator-settings' )
    };

    //
    // configure database
    //
    self.db = {
        users: new Datastore({filename: __dirname + '/data/users.db', autoload: true}),
        universe: {
            systems: new Datastore({filename: __dirname + '/data/universe/systems.db', autoload: true}),
            stars: new Datastore({filename: __dirname + '/data/universe/stars.db', autoload: true}),
            planets: new Datastore({filename: __dirname + '/data/universe/planets.db', autoload: true}),
            moons: new Datastore({filename: __dirname + '/data/universe/moons.db', autoload: true}),
            asteroids: new Datastore({filename: __dirname + '/data/universe/asteroids.db', autoload: true}),
            structures: new Datastore({filename: __dirname + '/data/universe/structures.db', autoload: true}),
        }
    }

    require( './db' )( self )
    .then( function() {

        //
        // configure models
        //
        self.models = {
            user: require('./models/user')( self )
        }

        //
        // configure rest server
        //
        self.express.use( bodyParser.json() )
        self.express.use( bodyParser.urlencoded({extended: true}) )
        self.routes = require( './routes/api' )( self, api )
        self.express.get( '/', function( req, res ) {
            res.send(self.status)
        })

        self.server = self.express.listen( http_port, function() {
            self.status.status = 'ready'
            console.log( "HTTP listening on port %s", http_port )
        })

        //
        // configure socket server
        //
        self.action_router = require( './routes/socket' )(self)
        self.socketServer = net.createServer( function(socket) {
            var connection = new SocketConnection( self, socket );
            self.connections.push(connection);
        })

        self.socketServer.listen( socket_port )
        console.log( "Persistent Socket listening on port %s", socket_port )

        self.simulation = require( './simulation' )( self )
    });
}

//
// #############################################################################
//

Application.prototype.getStatus = function() {
    var self = this;
    self.status.active_players = self.connections.length
    return self.status
}

//
// #############################################################################
//

Application.prototype.restart = function() {
    var self = this;
    for( var socket in self.connections ) {
        socket.disconnect();
    }

    for( var key in self.db.universe ) {
        self.db.universe[key].remove( {}, { multi: true } );
    }

    self.simulation.regenerateUniverse();
}

//
// #############################################################################
//

Application.prototype.broadcast = function( msg ) {
    var self = this;
    self.connections.forEach(function(conn) {
        conn.send(msg)
    })
}

//
// exports #####################################################################
//

module.exports = function( rootDir ) {
    global.App = new Application( rootDir )
    return global.App
}
