//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

//
// exports #####################################################################
//

module.exports = function( app, api ) {
    require( './' + api )(app);
}
