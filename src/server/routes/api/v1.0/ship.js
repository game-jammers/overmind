//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


module.exports = function( app, addRoute ) {

//
// system ######################################################################
//

addRoute.get( '/ship', function( req, res ) {
    var array = [];
    for( var key in app.db.ships ) {
        array.push( app.db.ships[key] );
    }

    if( req.query.class ) {
        var filtered = array.filter( function(ship) { return ship.class.toLowerCase() == req.query.class.toLowerCase(); } );
        res.send({ ships: filtered });
    }
    else {
        res.send({ ships: array });
    }
});

//
// #############################################################################
//

addRoute.get( '/ship/:id', function( req, res ) {
    res.send( app.db.ships[ req.params.id.toLowerCase() ] );
});

}
