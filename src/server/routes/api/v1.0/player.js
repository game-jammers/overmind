//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


module.exports = function( app, addRoute ) {

addRoute.get( '/player', function( req, res ) {
    var players = [];
    for( var i = 0; i < app.connections.length; ++i ) {
        var connection = app.connections[i];
        if( connection.user != null && connection.user != undefined ) {
            players.push({id: connection.user._id, name: connection.user.username});
        }
    }

    res.send({players: players});
});

//
// #############################################################################
//

addRoute.get( '/player/all', function( req, res ) {
    app.db.users.find( {}, function( err, users ) {
        res.send({users: users});
    });
});

//
// #############################################################################
//

addRoute.get( '/player/:id', function( req,res ) {
    var player = {};
    for( var i = 0; i < app.connections.length; ++i ) {
        var connection = app.connections[i];
        if( connection.user != null && connection.user != undefined ) {
            if( connection.user._id == req.params.id ||
                connection.user.username == req.params.id) {
                player = {
                    id: connection.user._id,
                    name: connection.user.username,
                    location: 'UNKNOWN'
                };
            }
        }
    }

    res.send(player);
});


}
