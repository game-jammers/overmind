//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


module.exports = function( app, addRoute ) {

//
// system ######################################################################
//

addRoute.get( '/universe/system', function( req, res ) {
    app.db.universe.systems.find( {}, function( err, systems ) {
        res.send({systems: systems});
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/system/:id', function( req, res ) {
    id = parseInt( req.params.id );
    app.db.universe.systems.findOne( {_id: id}, function( err, system ) {
        res.send(system);
    });
});

//
// star ########################################################################
//

addRoute.get( '/universe/star', function( req, res ) {
    app.db.universe.stars.find( {}, function( err, stars ) {
        res.send({stars: stars});
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/star/:id', function( req, res ) {
    id = parseInt( req.params.id );
    app.db.universe.stars.findOne( {_id: id}, function( err, star ) {
        res.send(star);
    });
});

//
// planet ######################################################################
//

addRoute.get( '/universe/planet', function( req, res ) {
    app.db.universe.planets.find( {}, function( err, planets ) {
        res.send({planets: planets});
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/planet/:id', function( req, res ) {
    id = parseInt( req.params.id );
    app.db.universe.planets.findOne( {_id: id}, function( err, planet ) {
        res.send(planet);
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/moon', function( req, res ) {
    app.db.universe.moons.find( {}, function( err, moons ) {
        res.send({moons: moons});
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/moon/:id', function( req, res ) {
    id = parseInt( req.params.id );
    app.db.universe.moons.findOne( {_id: id}, function( err, moon ) {
        res.send(moon);
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/asteroid', function( req, res ) {
    app.db.universe.asteroids.find( {}, function( err, asteroids ) {
        res.send({asteroids: asteroids});
    });
});

//
// #############################################################################
//

addRoute.get( '/universe/asteroid/:id', function( req, res ) {
    id = parseInt( req.params.id );
    app.db.universe.asteroids.findOne( {_id: id}, function( err, asteroid ) {
        res.send(asteroid);
    });
});

}
