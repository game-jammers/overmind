//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


module.exports = function( app ) {

var addRoute = {
    get: function( route, cb ) {
        app.express.get( '/api/v1.0' + route, cb );
    },

    post: function( route, cb ) {
        app.express.post( '/api/v1.0' + route, cb );
    }
}

require( './player' )( app, addRoute );
require( './ship' )( app, addRoute );
require( './universe' )( app, addRoute );

}
