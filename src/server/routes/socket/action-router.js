//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var Router = function( app ) {
    var self = this
    self.app = app
    self.routes = {}
}

Router.prototype.route = function( sender, packet ) {
    var self = this
    var handler = self.routes[ packet.action ]
    if( handler != null && handler != undefined ) {
        handler.handle( sender, packet )
    }
}

//
// exports #####################################################################
//

module.exports = function( app ) {
    return new Router(app)
}
