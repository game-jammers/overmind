//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var bcrypt = require('bcrypt')
  , rand = require('random-seed').create()

const saltRounds = 10;

module.exports = function(app, router, connection) {

//
// sign in #####################################################################
//

router.routes.signin = {
    handle: function( sender, packet ) {
        var res = {action: 'signin_res', success: false};
        app.models.user.findByUsernameAndPassword( packet.username, packet.password, function(err,user) {
            if(err) console.log("Failed to sign in: ",err);
            if( user != null ) {
                res.success = true;
                res.user = user.clientSanitize();
                sender.user = user;
            }

            sender.send(res);
        });
    }
}

//
// register ####################################################################
//

router.routes.register = {
    handle: function( sender, packet ) {
        var res = {action: 'register_res', success: false};
        app.models.user.create(packet.username, packet.password, function(err,user) {
            if(err) console.log("Failed to register: ",err);
            if( user != null ) {
                res.success = true;
            }
            sender.send(res);
        });
    }
}

//
// select ship #################################################################
//

router.routes.selectship = {
    handle: function( sender, packet ) {
        var res = {action: 'selectship_res', success: false};
        if( sender.user == null ) {
            console.log( 'select ship failed: no user' );
            return sender.send(res);
        }

        var selection = packet.selection.toLowerCase();
        if( app.db.ships[selection] == null || app.db.ships[selection] == undefined ) {
            console.log( app.db.ships );
            console.log( 'select ship failed: invalid ship - ', packet );
            return sender.send(res);
        }

        sender.user.ship = selection;
        sender.user.save( function( err, doc ) {
            res.success = true;
            res.user = sender.user.clientSanitize();
            return sender.send(res);
        });
    }
}

}
