//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var ActionRouter = require( './action-router' )

module.exports = function( app ) {
    var router = new ActionRouter(app)
    require('./auth')(app, router)
    return router;
}
