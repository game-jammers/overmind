//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var dataload = require( 'google-spreadsheet-to-json' )
  , Promise = require( 'bluebird' )

module.exports = function( app ) {
    return dataload({
        spreadsheetId: '1aftV9pWZHIoPsl4W6ADO7Uq_HxolfgSAzO_4jkY-vd0'
    }).then( function( res ) {
        var result = {};
        for( var key in res ) {
            var doc = res[key];
            result[ doc.id ] = doc;
        }
        app.db.ships = result;
    });
}
