//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var Promise = require( 'bluebird' )

module.exports = function( app ) {
    console.log( 'Loading google spreadsheet design time data...' );
    return require( './ships' )( app );
}
