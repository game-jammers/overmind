//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

require( './app' )( __dirname )

const http_port = 9997
const socket_port = 9996
const api = 'v1.0'

App.start( http_port, socket_port, api )
