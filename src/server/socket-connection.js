//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var SmartBuffer = require( 'smart-buffer' ).SmartBuffer
  , lastId = 0

const kHeartbeatPeriod = 2.0;

var SocketConnection = function( app, socket ) {
    var self = this;
    self.socket = socket;
    self.id = ++lastId;
    self.active = false;

    self.socket.on( 'end', function() {
            self.disconnect( socket );
        })

    //
    // setup the data callback
    //
    const headerSize = 8;
    var buffer = new SmartBuffer();
    var header = null;

    self.socket.on( 'data', function(data) {
            buffer.writeBuffer(data);
            if( header == null ) {
                if( buffer.remaining() >= headerSize ) {
                    header = {
                        id: buffer.readInt32LE(),
                        payloadSize: buffer.readInt32LE()
                    }
                }
            }

            if( header != null && header.payloadSize <= buffer.remaining()) {
                var jsonString = buffer.readString(header.payloadSize);
                app.action_router.route( self, JSON.parse(jsonString) );
                header = null;
            }
        })

    self.socket.on( 'error', function( exception ) {
        self.disconnect( socket );
    });

    self.send( { action: 'debug', msg: 'welcome to the server' } );
    (function heartbeat() {
        setTimeout( function() {
            self.send( { action: 'heartbeat' } );
            if( self.active ) {
                heartbeat();
            }
        }, kHeartbeatPeriod );
    }());
}

SocketConnection.prototype.send = function( msg ) {
    var self = this;

    var packet = new SmartBuffer();
    var payload = JSON.stringify(msg);
    packet.writeInt32LE(-2); // -2 == JsonPacket
    packet.writeInt32LE(payload.length);
    packet.writeString(payload);
    self.socket.write(packet.toBuffer());
}

SocketConnection.prototype.disconnect = function( socket ) {
    var self = this;
    self.active = false;
    for( var i = 0; i < App.connections.length; ++i ) {
        var connection = App.connections[i];
        if( connection.id == self.id ) {
            App.connections.splice( App.connections[i], 1 );
        }
    }

    var msg = { action: "leave", id: self.id };
    App.broadcast(msg);
}

module.exports = SocketConnection;
