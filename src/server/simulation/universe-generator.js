//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var Rand = require( 'random-seed' )
  , uuid = require( 'uuid/v4' )
  , StringDecoder = require( 'string_decoder' ).StringDecoder
  , nameGenerator = require( './name-generator' )()

var generateRandomSeed = function() {
    var decoder = new StringDecoder('utf8');
    return decoder.end( uuid() );
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

//
// settings ####################################################################
//

var buildSingle = function( rand, settings, distOffset, processcb ) {
    if( settings.lastId === undefined ) {
        settings.lastId = -1;
    }

    var body = {
        _id: ++settings.lastId,
        size: {
            value: rand.intBetween( settings.size.min, settings.size.max ),
            unit: settings.size.unit
        },
        dist: {
            value: rand.intBetween( settings.spacing.min, settings.spacing.max ) + distOffset,
            unit: settings.size.unit
        }
    }

    if( processcb != null && processcb != undefined ) {
        processcb( body );
    }

    return body;
}

var build = function( rand, settings, distOffset, processcb ) {
    var result = [];

    var count = rand.intBetween( settings.count.min, settings.count.max );

    var lastDist = 0;
    for( var i = 0; i < count; ++i ) {
        var body = buildSingle( rand, settings, distOffset, processcb )
        distOffset += body.dist.value;
        result.push(body);
    }

    return result;
}

//
// UniverseGenerator ###########################################################
//

var UniverseGenerator = function() {
    var self = this;
}

//
// constants ###################################################################
//

UniverseGenerator.DistanceUnits = {
    Meter: "Meter",
    Kilometer: "Kilometer",
    AstronomicalUnit: "AstronomicalUnit",
    Lightyear: "Lightyear",
    Parsec: "Parsec"
}

//
// #############################################################################
//

UniverseGenerator.Factors = {
    Meter: 1.0,
    Kilometer: 1000.0,
    Megameter: 1000000.0,
    AstronomicalUnit: 149600000000.0,
    Lightyear: 9461000000000000.0,
    Parsec: 30860000000000000.0,
}

//
// #############################################################################
//

UniverseGenerator.ConvertUnits = function( value, from, to ) {
    var mult = UniverseGenerator.Factors[to];
    var div = UniverseGenerator.Factors[from];

    if( mult == null || mult == undefined || div == null || div == undefined ) {
        return 0;
    }

    return ( value * mult / div );
}

//
// #############################################################################
//

UniverseGenerator.prototype.generate = function( settings ) {
    var self = this;

    self.settings = settings || UniverseGenerator.DefaultSettings;
    self.settings.seed = self.settings.seed || generateRandomSeed();
    self.rand = Rand.create(self.seed);

    // build all the systems
    var universe = {
        systems: [],
        stars: [],
        planets: [],
        asteroids: [],
        structures: [],
        moons: []
    }

    settings.systems.lastId = settings.systems.lastId || -1;
    var totalSystems = self.rand.intBetween( settings.systems.count.min, settings.systems.count.max );
    var maxpos = totalSystems * settings.systems.spacing.max * 2.0;
    for( var i = 0; i < totalSystems; ++i ) {
        var system = {
            _id: ++settings.systems.lastId,
            name: nameGenerator.systemName(),
            explored: false,
            owner: "neutral",
            children: {
                stars: [],
                planets: [],
                asteroids: [],
                structures: [],
            },
            connections: [],
            location: {
                x: { value: self.rand.intBetween( maxpos * -1, maxpos ), unit: settings.systems.spacing.unit },
                y: { value: self.rand.intBetween( settings.systems.depth.min, settings.systems.depth.max ), unit: settings.systems.spacing.unit },
                z: { value: self.rand.intBetween( maxpos * -1, maxpos ), unit: settings.systems.spacing.unit }
            }
        };

        universe.systems.push( system );

        //
        // generate stars
        //
        var outerStar = null;
        var starNum = 0;
        universe.stars = universe.stars.concat( build( self.rand, settings.stars, 0, function(star) {
            star.name = nameGenerator.starName( system.name, ++starNum ),
            system.children.stars.push( star._id );
            star.system = system._id;
            outerStar = star;
        }));

        //
        // generate planets
        //
        var distance = UniverseGenerator.ConvertUnits( outerStar.dist.value, settings.stars.size.unit, settings.planets.size.unit )
        var lastPlanetDist = 0;
        var planetNum = 0;
        universe.planets = universe.planets.concat(build( self.rand, settings.planets, distance, function(planet) {
            planet.name = nameGenerator.planetName( system.name, ++planetNum ),
            system.children.planets.push(planet._id);
            planet.system = system._id;

            // build moons
            planet.moons = [];
            var moonNum = 0;
            universe.moons = universe.moons.concat(build( self.rand, settings.moons, 0, function(moon) {
                moon.name = planet.name + ' Moon ' + nameGenerator.numeral( ++moonNum )
                planet.moons.push(moon._id);
                moon.planet = planet._id;
            }));

            // if this is the first planet early out
            if( lastPlanetDist == 0 ) {
                lastPlanetDist = planet.dist.value;
                return;
            }

            // look for big gaps to place asteroid fields
            var asteroidNum = 0;
            var asteroidDistance = UniverseGenerator.ConvertUnits( planet.dist.value, settings.planets.size.unit, settings.asteroids.size.unit )
            var gap = asteroidDistance - lastPlanetDist;
            if( gap > settings.asteroids.spacing.min && system.children.asteroids.length < settings.asteroids.count.max ) {
                var asteroid = buildSingle( self.rand, settings.asteroids, asteroidDistance );
                asteroid.name = planet.name + ' Asteroid Field ' + nameGenerator.numeral( ++asteroidNum );
                asteroid.system = system._id;
                system.children.asteroids.push( asteroid._id );
                universe.asteroids.push(asteroid);
            }

            lastPlanetDist = asteroidDistance;
        }));
    };

    var mag2 = function( sys ) {
        return ( sys.location.x.value * sys.location.x.value +
                 sys.location.y.value * sys.location.y.value +
                 sys.location.z.value * sys.location.z.value );
    }

    var dist2 = function( sys1, sys2 ) {
        var sub = {};
        sub.x = sys1.location.x.value - sys2.location.x.value;
        sub.y = sys1.location.y.value - sys2.location.y.value;
        sub.z = sys1.location.z.value - sys2.location.z.value;
        return ( sub.x * sub.x + sub.y * sub.y + sub.z * sub.z );
    }

    var search = function( collection ) {
        for( var i = 0; i < collection.length; ++i ) {
            if( predicate(collection[i]) ) {
                return i;
            }
        }

        return -1;
    }

    var iscon = function( sys1, sys2 ) {
        return search( sys1, function(sys) { return sys._id == sys2._id; } ) != -1;
    }

    var connect = function( sys1, sys2 ) {
        if(iscon( sys1, sys2 )) return;
        sys1.connections.push( sys2._id );
        sys2.connections.push( sys1._id );
    }

    var shouldConnect = function( sys1, sys2 ) {
        if(sys1._id == sys2._id) return false;
        if(sys1.connections.indexOf(sys2._id) != -1 ) return false;

        var dist = dist2(sys1, sys2);
        for( var i = 0; i < universe.systems.length; ++i ) {
            var test = universe.systems[i];
            if( test._id != sys1._id && test._id != sys2._id ) {
                if( dist2(sys1,test) < dist && dist2(sys2,test) < dist ) {
                    return false;
                }
            }
        }
        return true;
    }

    for( var i = 0; i < universe.systems.length; ++i ) {
        var system = universe.systems[i];
        for( var j = 0; j < universe.systems.length; ++j ) {
            var check = universe.systems[j];
            if( shouldConnect( system, check ) ) {
                connect( system, check );
            }
        }
    }

    return universe;
}

//
// exports #####################################################################
//

module.exports = function() {
    return new UniverseGenerator();
}
