//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var Generator = require( './universe-generator' )()
  , rand = require( 'random-seed' ).create()

var Simulation = function() {
    var self = this;

    App.db.universe.systems.count({}, function( err, count ) {
        if( count <= 0 ) {
            self.regenerateUniverse();
        }
    });

    (function updateLoop() {
        self.update();
        setTimeout( updateLoop, 1000 );
    }());
}

//
// update loop #################################################################
//

Simulation.prototype.update = function() {
    var self = this;
}

//
// #############################################################################
//

Simulation.prototype.regenerateUniverse = function() {
    var self = this;

    var universe = Generator.generate(App.config.generator);
    App.db.universe.systems.insert( universe.systems );
    App.db.universe.stars.insert( universe.stars );
    App.db.universe.planets.insert( universe.planets );
    App.db.universe.moons.insert( universe.moons );
    App.db.universe.asteroids.insert( universe.asteroids );
    App.db.universe.structures.insert( universe.structures );

    App.db.universe.systems.find( { owner: 'neutral' }, function( err, systems ) {
        var half = Math.floor((systems.length-1)/2);
        var playerSystem = systems[rand.intBetween(0, half-1)];
        var overmindSystem = systems[rand.intBetween(half,systems.length-1)];

        playerSystem.owner = 'player';
        playerSystem.explored = true;
        App.db.universe.systems.update({_id:playerSystem._id}, playerSystem);

        overmindSystem.owner = 'overmind';
        App.db.universe.systems.update({_id:overmindSystem._id}, overmindSystem);
    });
}

//
// #############################################################################
//

Simulation.prototype.spawnPlayer = function( user, cb ) {
    var self = this;

    // choose from a list of given systems which will be this players new home
    var doSpawnPlayer = function( user, systems ) {
        var system = systems[ rand.range(systems.length-1) ];
        system.owner = 'player';
        system.explored = true;
        user.location = system._id;
        App.db.universe.systems.update({_id:system._id}, system, function( err, doc ) {
            App.db.users.update({_id:user._id}, user, function( err, userdoc ) {
                cb( err, user );
            });
        });
    };

    // look for an explored system to place the user, otherwise find a neutral system.
    // if no explored or neutral systems exist, GAME OVER MAN
    var findPlayerHome = function( user ) {
        App.db.universe.systems.find( {explored: true}, function( err, systems ) {
            if( systems == null || systems.length == 0 ) {
                App.db.universe.systems.find( {owner: "neutral"}, function( err, systems ) {
                    if( !err && systems.length <= 0 ) {
                        self.gameOver();
                    }
                    else {
                        doSpawnPlayer( user, systems );
                    }
                });
            }
            else {
                doSpawnPlayer( user, systems );
            }
        });
    };

    // if the user already has a valid location use that, otherwise try and find them a new home
    if( user.location != null && user.location != undefined && user.location != -1) {
        App.db.universe.systems.find( {_id: user.location }, function( err, system ) {
            if( system.owner == 'player' ) {
                cb( err, user );
            }
            else {
                findPlayerHome( user );
            }
        });
    }
    else {
        findPlayerHome( user );
    }
}

//
// #############################################################################
//

Simulation.prototype.gameOver = function() {
    console.log( "GAME OVER" );
    App.broadcast({action:"game_over"});
    App.restart();
}

//
// exports #####################################################################
//

module.exports = function( app ) {
    return new Simulation();
}
