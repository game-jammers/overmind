//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var rand = require( 'random-seed' ).create()

const kLetterStrings = [
    "am",
    "an",
    "at",
    "ba",
    "be",
    "bl",
    "ch",
    "ci",
    "cl",
    "cr",
    "de",
    "di",
    "dr",
    "eb",
    "el",
    "en",
    "er",
    "es",
    "ev",
    "ew",
    "ha",
    "ia",
    "in",
    "ip",
    "is",
    "it",
    "iv",
    "la",
    "lu",
    "ma",
    "me",
    "mo",
    "na",
    "nc",
    "nd",
    "ne",
    "ni",
    "nn",
    "no",
    "nu",
    "on",
    "or",
    "ou",
    "pt",
    "rc",
    "re",
    "ri",
    "rm",
    "rr",
    "ry",
    "sc",
    "se",
    "st",
    "te",
    "ti",
    "tt",
    "ue",
    "un",
    "us",
    "va",
    "ve",
    "wd",
    "we",
    "wo",
    "wu",
    "xa",
    "xe",
    "xi",
    "xu",
    "ya",
    "ye",
    "yi",
    "yo",
    "yu",
];

const kNumberStrings = [
    "I",
    "II",
    "III",
    "IV",
    "V",
    "VI",
    "VII",
    "VIII",
    "IX",
    "X",
]

const kLatinNumbers = [
    "Prime",
    "Secunde",
    "Tertie",
    "Quarte",
    "Quinte",
    "Sexte",
    "Septime",
    "Octante",
    "Novante",
    "Decime"
]

var pick = function( arr ) {
    return arr[ rand.range( arr.length ) ];
}

var pickLetters = function() {
    return pick( kLetterStrings );
}

var pickNumeral = function() {
    return pick( kNumberStrings );
}

var generateSystemName = function() {
    var numberOfLetterStrings = rand.intBetween(3,5);

    var name = '';
    for( var i = 0; i < numberOfLetterStrings; ++i ) {
        name += pickLetters();
    }

    name = name.charAt(0).toUpperCase() + name.slice(1);

    return name;
}

var generateNumeral = function( planetNum ) {
    var num10s = Math.floor( planetNum / 10 );
    var result = '';
    for( var i = 0; i < num10s; ++i ) {
        result += 'X'
    }

    var rem = planetNum % 10;
    if( rem == 0 ) return result

    result += kNumberStrings[rem-1];

    return result;
}

var generatePlanetName = function( systemName, num ) {
    return systemName + ' ' + generateNumeral( num );
}

var generateStarName = function( systemName, starNum ) {
    if( starNum > 10 ) return systemName + ' Star' + generateNumeral( starNum );
    if( starNum == 0 ) starNum = 1;
    return systemName + ' ' + kLatinNumbers[ starNum-1 ];
}

module.exports = function() {
    return {
        numeral: generateNumeral,
        systemName: generateSystemName,
        planetName: generatePlanetName,
        starName: generateStarName
    };
}
