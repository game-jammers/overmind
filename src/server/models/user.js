//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

var bcrypt = require( 'bcrypt' )
  , merge = require( 'merge' )

const SALT_ROUNDS = 10;

module.exports = function( app ) {
    var User = function() {
        var self = this;
        self.username = '';
        self.password = '';
        self.location = -1;
        self.ship = 'none';
    }

    User.createFromRecord = function( record ) {
        var result = new User();
        result = merge.recursive(true, result, record);
        return result;
    }

    User.prototype.clientSanitize = function() {
        var self = this;
        var result = merge.recursive(true, self );
        result.id = result._id;
        delete result._id;
        delete result.password;
        return result;
    }

    var createUser = function( username, password, cb ) {
        bcrypt.genSalt( SALT_ROUNDS, function( err, salt ) {
            if( err ) return cb( err, null );
            bcrypt.hash( password, salt, function( err, hash ) {
                if( err ) return cb( err, null );
                var user = new User();
                user.username = username;
                user.password = hash;
                app.db.users.insert( user, function( err, doc ) {
                    if( err ) return cb( err, null );
                    user._id = doc._id;
                    app.simulation.spawnPlayer( user, function( err, doc ) {
                        user.save( cb );
                    });
                });
            });
        });
    }

    //
    // #########################################################################
    //

    User.findById = function( id, cb ) {
        app.db.users.findOne( {id: id}, function( err, doc ){
            if( err || doc == null ) {
                return cb( err, null );
            }
            else {
                return cb( null, User.createFromRecord(doc) );
            }
        });
    }

    User.findByUsername = function( username, cb ) {
        app.db.users.findOne( {username: username}, function( err, doc ){
            if( err || doc == null ) {
                return cb( err, null );
            }
            else {
                return cb( null, User.createFromRecord(doc) );
            }
        });
    }

    User.findByUsernameAndPassword = function( username, password, cb ) {
        User.findByUsername( username, function( err, user ) {
            if( err ) return cb( err, null );
            if( user == null ) return cb({msg:'No such user'}, null );
            bcrypt.compare(password, user.password, function(err,equal) {
                if(equal) {
                    return cb(null,user);
                }
                else {
                    return cb({msg:'Incorrect password'}, null);
                }
            });
        });
    }

    User.create = function( username, password, cb ) {
        User.findByUsername( username, function( err, doc ) {
            if( err ) {
                return cb( err, null );
            }
            else if( doc != null ) {
                return cb( {msg: 'Already exists'}, null );
            }
            else {
                createUser( username, password, cb );
            }
        });
    }

    User.prototype.save = function( cb ) {
        var self = this;
        app.db.users.update( {id: self._id}, self, function( err, doc ){
            if( err ) cb( err, null );
            cb( null, self );
        });
    }


    return User;
}
