//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class SolarSystem
        : MonoBehaviour
    {
        // private types ///////////////////////////////////////////////////////
        public struct Connection
        {
            public SolarSystem system;
        }

        // members /////////////////////////////////////////////////////////////
        public UniverseMode mode                                 { get; private set; }
        public SolarSystemData data                              { get; private set; }
        public Billboard highlight;
        public bool isHighlight                                  { get; private set; }

        public List<Connection> connections                      { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public void Initialize( UniverseMode _mode )
        {
            mode = _mode;
            isHighlight = false;
            connections = new List<Connection>();
            foreach( SolarSystemData connection in data.connections )
            {
                SolarSystem system = mode.FindSolarSystem( (SolarSystem c)=>{
                    return c.data.id == connection.id;
                });

                if( system != null )
                {
                    Connection newConn = new Connection();
                    newConn.system = system;
                    connections.Add(newConn);
                }
            }
        }

        // public methods //////////////////////////////////////////////////////
        public static Vector3 StellarToScenePos( StellarLocation loc )
        {
            return loc.ToVector3( UnitType.Parsec ) * 0.001f;
        }

        public void SetData( SolarSystemData _data )
        {
            data = _data;
            gameObject.name = data.name;
        }

        public void SetHighlight( bool set )
        {
            isHighlight = set;
            highlight.gameObject.SetActive( isHighlight );
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            highlight.gameObject.SetActive( false );
        }
    }
}
