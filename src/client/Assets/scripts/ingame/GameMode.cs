//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Threading;

namespace GameJammers.Overmind
{
    public abstract class GameMode
    {
        // types ///////////////////////////////////////////////////////////////
        public enum State
        {
            Inactive,
            Activating,
            Active,
            Deactivating,
        }

        // members /////////////////////////////////////////////////////////////
        public InGameSceneController controller                  { get; private set; }
        public NetworkManager network                            { get { return controller.network; } }
        public State state                                       { get; protected set; }

        // constructor / initializer ///////////////////////////////////////////
        public virtual void Initialize( InGameSceneController _controller )
        {
            controller = _controller;
            state = State.Inactive;
        }

        // public methods //////////////////////////////////////////////////////
        public virtual void Activate()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Deactivate()
        {
        }

        public virtual void Render()
        {
        }
    }
}
