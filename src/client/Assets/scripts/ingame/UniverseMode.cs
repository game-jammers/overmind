//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace GameJammers.Overmind
{
    [System.Serializable]
    public class UniverseMode
        : GameMode
    {
        // types ///////////////////////////////////////////////////////////////
        public enum SpaceAction
        {
        	CameraX,
        	CameraY,
        	OrbitModifier,
        	PanModifier,
        	Zoom,
        	Select,
        };

        private class Segment
        {
            public SolarSystem p1;
            public SolarSystem p2;

            public Segment( SolarSystem _p1, SolarSystem _p2 )
            {
                p1 = _p1;
                p2 = _p2;
            }

            public bool Compare( Segment other )
            {
                return ( p1 == other.p1 && p2 == other.p2 ) ||
                       ( p1 == other.p2 && p2 == other.p1 );
            }

            public bool Compare( SolarSystemData d1, SolarSystemData d2 )
            {
                return ( p1.data == d1 && p2.data == d2 ) ||
                       ( p1.data == d2 && p2.data == d1 );
            }
        }

        // members /////////////////////////////////////////////////////////////
        public InputRouter<SpaceAction> input			         { get; private set; }
        public SolarSystem activeSystem                          { get; private set; }
        public SolarSystem selectedSystem                        { get; private set; }
        public SolarSystem[] systems                             { get { return solarSystems.ToArray(); } }
        public SolarSystemData[] path                            { get; private set; }

        [SerializeField] Material lineMaterial                   = null;
        [SerializeField] Material pathLineMaterial               = null;
        public Vector2 rotateSpeed								 = Vector2.one;
        public FloatRange rotateXRange			                 = new FloatRange( -90, 90f );
        public float panSpeed                                    = 10.0f;
        public float zoomSpeed                                   = 10.0f;

        private List<SolarSystem> solarSystems                   = new List<SolarSystem>();
		private Vector2 eulerRot								 = Vector2.zero;

        [SerializeField] SolarSystem solarSystemPrefab           = null;
        [SerializeField] UniverseOverlay overlay                 = null;

        private System.Object pathLock                           = new System.Object();
        private SolarSystemData[] calculatedPath                 = null;

        private Segment[] segments                               = null;

        // constructor / initializer ///////////////////////////////////////////
        public override void Initialize( InGameSceneController _controller )
        {
            base.Initialize( _controller );
            path = null;
            LoadInputs();
        }

        // public interface methods ////////////////////////////////////////////
        public SolarSystem FindSolarSystem( System.Func<SolarSystem,bool> predicate )
        {
            foreach( SolarSystem system in solarSystems )
            {
                if( predicate( system ) ) return system;
            }
            return null;
        }

        public void CalculatePath()
        {
            if( activeSystem == null || selectedSystem == null )
            {
                path = null;
                return;
            }

            Database.universe.GetPath( activeSystem.data, selectedSystem.data, (SolarSystemData[] newPath)=>{
                lock(pathLock)
                {
                    calculatedPath = newPath;
                }
            });
        }

        // public methods //////////////////////////////////////////////////////
        public override void Activate()
        {
            overlay.universe = this;
            overlay.Hide( 0.0f );
            GameManager.instance.dialog.loadingOverlay.loading = 0.0f;
            GameManager.instance.dialog.loadingOverlay.Show( 0.0f );
            state = State.Activating;
            Database.universe.Load( (bool success)=> {
                DebugUtility.Log( "Finished universe load" );
                DebugUtility.Assert( success, "Failed to load UniverseDb" );
                controller.StartCoroutine( BuildUniverse() );
            });
        }

        public override void Update()
        {
            if( state == State.Activating )
            {
                GameManager.instance.dialog.loadingOverlay.loading = Database.universe.loading;
            }
            else if( state != State.Active ) return;

            if( calculatedPath != null )
            {
                lock(pathLock)
                {
                    path = (SolarSystemData[])calculatedPath.Clone();
                    calculatedPath = null;
                }

                overlay.RefreshPath();
            }

            float mousex = input.GetAxis( SpaceAction.CameraX ) * rotateSpeed.x * Time.deltaTime;
            float mousey = input.GetAxis( SpaceAction.CameraY ) * rotateSpeed.y * Time.deltaTime;

            if( input.GetKey( SpaceAction.OrbitModifier ) )
            {
                eulerRot += new Vector2( -mousey, mousex );
				eulerRot.x = rotateXRange.Clamp( eulerRot.x );
				eulerRot.y = eulerRot.y % 360.0f;
				controller.cam.transform.rotation = Quaternion.Euler( eulerRot.x, eulerRot.y, 0.0f );
            }
            else if( input.GetKey( SpaceAction.PanModifier ) )
            {
                controller.cam.transform.position += controller.cam.transform.rotation * new Vector2( -mousex, -mousey ) * panSpeed;
            }

            if( input.GetKeyDown( SpaceAction.Select ) )
            {
                SolarSystem selection = controller.cam.MousePick<SolarSystem>( input.mousePos );
                SelectSystem( selection );

                overlay.Refresh();
            }

            controller.cam.transform.position += controller.cam.transform.forward * input.GetAxis( SpaceAction.Zoom ) * zoomSpeed;
        }

        public override void Deactivate()
        {
            overlay.Hide( 0.5f );
            state = State.Inactive;
        }

        public void SelectSystem( SolarSystem selection )
        {
            if( selectedSystem != null )
                selectedSystem.SetHighlight( false );

            selectedSystem = selection;

            if( selectedSystem != null )
            {
                selectedSystem.SetHighlight( true );
                CalculatePath();
            }
            else
            {
                path = null;
            }
        }

        public bool InPath( SolarSystemData data )
        {
            return (path != null && path.Contains( data ));
        }

        // drawing /////////////////////////////////////////////////////////////
        public override void Render()
        {
            if( segments == null ) return;

            List<Segment> pathSegs = new List<Segment>();

            if( path != null && path.Length > 1 )
            {
                pathLineMaterial.SetPass(0);
                GL.Begin(GL.LINES);
                    for( int i = 0; i < path.Length-1; ++i )
                    {
                        Segment segment = FindSegment( path[i], path[i+1] );
                        Vector3 p1 = segment.p1.transform.position;
                        Vector3 p2 = segment.p2.transform.position;
                        GL.Vertex3( p1.x, p1.y, p1.z );
                        GL.Vertex3( p2.x, p2.y, p2.z );
                        pathSegs.Add( segment );
                    }
                GL.End();
            }

            lineMaterial.SetPass(0);
            GL.Begin(GL.LINES);
                foreach( Segment segment in segments )
                {
                    if( pathSegs.Find( (fseg)=>{ return fseg.Compare(segment); } ) == null )
                    {
                        Vector3 p1 = segment.p1.transform.position;
                        Vector3 p2 = segment.p2.transform.position;
                        GL.Vertex3( p1.x, p1.y, p1.z );
                        GL.Vertex3( p2.x, p2.y, p2.z );
                    }
                }
            GL.End();
        }

        // private methods /////////////////////////////////////////////////////
        private Segment FindSegment( SolarSystem p1, SolarSystem p2 )
        {
            return FindSegment( p1.data, p2.data );
        }

        private Segment FindSegment( SolarSystemData d1, SolarSystemData d2 )
        {
            return segments.Find( (Segment fseg)=>{ return fseg.Compare(d1,d2); } );
        }

        private void LoadInputs()
		{
			input = new InputRouter<SpaceAction>();
			input.Bind( SpaceAction.CameraX, InputAction.FromAxis( InputAction.Axis.MouseHorizontal, false, 1.0f ) );
			input.Bind( SpaceAction.CameraY, InputAction.FromAxis( InputAction.Axis.MouseVertical, false, 1.0f ) );
			input.Bind( SpaceAction.OrbitModifier, InputAction.FromKey( KeyCode.Mouse1 ) );
			input.Bind( SpaceAction.PanModifier, InputAction.FromKey( KeyCode.Mouse2 ) );
			input.Bind( SpaceAction.Zoom, InputAction.FromAxis( InputAction.Axis.MouseWheel, true, 1.0f, true ) );
			input.Bind( SpaceAction.Select, InputAction.FromKey( KeyCode.Mouse0, true ) );
		}

        private IEnumerator BuildUniverse()
        {
            if( systems.Length == 0 ) yield return null;

            List<Segment> pathSegments = new List<Segment>();

            for( int i = 0; i < Database.universe.systems.Length; ++i )
            {
                SolarSystemData system = Database.universe.systems[i];
                SolarSystem obj = GameObject.Instantiate( solarSystemPrefab );

                obj.SetData( system );
                if( system.id == GameManager.instance.user.location )
                {
                    activeSystem = obj;
                    SelectSystem( activeSystem );
                }

                obj.transform.position = SolarSystem.StellarToScenePos( system.location );
                solarSystems.Add( obj );
                if( i % 20 == 0 )
                {
                    yield return new WaitForSeconds(0.0f);
                }
            }

            foreach( SolarSystem system in solarSystems )
            {
                system.Initialize( this  );
                foreach( SolarSystem.Connection conn in system.connections )
                {
                    Segment segment = new Segment( system, conn.system );
                    if( pathSegments.Find( (Segment fseg)=>{ return fseg.Compare(segment); } ) == null )
                    {
                        pathSegments.Add(segment);
                    }
                }
            }

            segments = pathSegments.ToArray();

            controller.transform.position = activeSystem.transform.position + ( Vector3.forward * -10.0f );

            GameManager.instance.dialog.loadingOverlay.Hide( 1.0f );
            state = State.Active;
        }
    }
}
