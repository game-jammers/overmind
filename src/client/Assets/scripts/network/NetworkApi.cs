//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{

    public partial class NetworkApi
    {
        // members /////////////////////////////////////////////////////////////
        public NetworkManager netman                             { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public NetworkApi( NetworkManager manager )
        {
            netman = manager;
        }

        // public methods //////////////////////////////////////////////////////
        public Messages.ServerStatus GetStatus()
        {
            // get a json object from the api endpoint, construct the result from it
            return new Messages.ServerStatus(netman.MakeApiRequest( "/" ));
        }
    }
}
