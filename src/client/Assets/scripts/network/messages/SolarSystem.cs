//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using GameJammers.Overmind;

namespace GameJammers.Overmind.Messages
{
    public class SolarSystem
        : StellarObject
    {
        // types ///////////////////////////////////////////////////////////////
        public class Children
            : IJsonSerializable
        {
            public int[] stars;
            public int[] planets;
            public int[] asteroids;
            public int[] structures;


            public JsonObject ToJson()
            {
                JsonObject result = new JsonObject();
                result["stars"] = stars;
                result["planets"] = planets;
                result["asteroids"] = asteroids;
                result["structures"] = structures;
                return result;
            }

            public void FromJson( JsonObject json )
            {
                stars = json.GetField<int[]>("stars");
                planets = json.GetField<int[]>("planets");
                asteroids = json.GetField<int[]>("asteroids");
                structures = json.GetField<int[]>("structures");
            }
        };

        // members /////////////////////////////////////////////////////////////
        public Children children                                 { get; private set; }
        public int[] connections                                 { get; private set; }

        // json ////////////////////////////////////////////////////////////////
        public override JsonObject ToJson()
        {
            JsonObject result = base.ToJson();
            result["children"] = children;
            result["connections"] = connections;
            return result;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson( json );
            children = json.GetField<Children>( "children" );
            connections = json.GetField<int[]>("connections");

        }
    }
}
