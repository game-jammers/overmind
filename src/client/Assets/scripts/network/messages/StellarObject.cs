//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using GameJammers.Overmind;

namespace GameJammers.Overmind.Messages
{
    public abstract class StellarObject
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public int id                                            { get; private set; }
        public string name                                       { get; private set; }
        public StellarLocation location                          { get; private set; }

        // json ////////////////////////////////////////////////////////////////
        public virtual JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["_id"] = id;
            json["name"] = name;
            json["location"] = location;
            return json;
        }

        public virtual void FromJson( JsonObject json )
        {
            id = json.GetField<int>("_id");
            name = json.GetField<string>( "name" );
            location = json.GetField<StellarLocation>( "location" );
        }
    }
}
