//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind.Messages
{
    public class Player
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public string id;
        public string username;

        // json methods ////////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            throw new System.NotImplementedException( "GameStatus is receive only should not be sent" );
        }

        public void FromJson( JsonObject json )
        {
            id = json.GetField<string>("id");
            username = json.GetField<string>("name");
        }
    }
}
