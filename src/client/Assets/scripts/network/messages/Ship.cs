//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using GameJammers.Overmind;

namespace GameJammers.Overmind.Messages
{
    public class Ship
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public string id                                         { get; private set; }
        public string name                                       { get; private set; }
        public string description                                { get; private set; }
        public ShipClass shipClass                               { get; private set; }
        public int hull                                          { get; private set; }
        public int dps                                           { get; private set; }
        public int warpSpeed                                     { get; private set; }
        public int cargo                                         { get; private set; }

        // json ////////////////////////////////////////////////////////////////
        public virtual JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["id"] = id;
            json["name"] = name;
            json["desc"] = description;
            json["class"] = shipClass;
            json["hull"] = hull;
            json["dps"] = dps;
            json["warp"] = warpSpeed;
            json["cargo"] = cargo;
            return json;
        }

        public virtual void FromJson( JsonObject json )
        {
            id = json.GetField<string>("id");
            name = json.GetField<string>("name");
            description = json.GetField<string>("desc");
            shipClass = json.GetField<ShipClass>("class");
            hull = json.GetField<int>("hull");
            dps = json.GetField<int>("dps");
            warpSpeed = json.GetField<int>("warp");
            cargo = json.GetField<int>("cargo");
        }
    }
}
