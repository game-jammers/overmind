//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind.Messages
{
    public class ServerStatus
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public string status                                     { get; private set; }
        public int apiPort                                       { get; private set; }
        public int pushPort                                      { get; private set; }
        public string apiVersion                                 { get; private set; }
        public int activePlayers                                 { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public ServerStatus( JsonObject json )
        {
            if( json != null )
            {
                FromJson( json );
            }
        }

        // json methods ////////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            throw new System.NotImplementedException( "GameStatus is receive only should not be sent" );
        }

        public void FromJson( JsonObject json )
        {
            status = json.GetField<string>("status");
            apiPort = json.GetField<int>("api_port");
            pushPort = json.GetField<int>("push_port");
            apiVersion = json.GetField<string>("api");
            activePlayers = json.GetField<int>("active_players");
        }
    }
}
