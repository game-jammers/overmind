//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    public partial class NetworkApi
    {
        //
        // solar system ########################################################
        //

        public Messages.SolarSystem[] GetSolarSystems()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/system" ).GetField<Messages.SolarSystem[]>("systems");
        }

        public Messages.SolarSystem GetSolarSystem( int id )
        {
            return JsonObject.Create<Messages.SolarSystem>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/system/{0}", id ) ) );
        }

        //
        // star ################################################################
        //

        public Messages.Star[] GetStars()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/star" ).GetField<Messages.Star[]>("stars");
        }

        public Messages.Star GetStar( int id )
        {
            return JsonObject.Create<Messages.Star>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/star/{0}", id ) ) );
        }

        //
        // planet ##############################################################
        //

        public Messages.Planet[] GetPlanets()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/planet" ).GetField<Messages.Planet[]>("planets");
        }

        public Messages.Planet GetPlanet( int id )
        {
            return JsonObject.Create<Messages.Planet>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/planet/{0}", id ) ) );
        }

        //
        // moon ################################################################
        //

        public Messages.Moon[] GetMoons()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/moon" ).GetField<Messages.Moon[]>("moons");
        }

        public Messages.Moon GetMoon( int id )
        {
            return JsonObject.Create<Messages.Moon>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/moon/{0}", id ) ) );
        }

        //
        // asteroidField #######################################################
        //

        public Messages.AsteroidField[] GetAsteroidFields()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/asteroid" ).GetField<Messages.AsteroidField[]>("asteroids");
        }

        public Messages.AsteroidField GetAsteroidField( int id )
        {
            return JsonObject.Create<Messages.AsteroidField>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/asteroid/{0}", id ) ) );
        }

        //
        // structure ###########################################################
        //

        public Messages.Structure[] GetStructures()
        {
            return netman.MakeApiRequest( "/api/v1.0/universe/structure" ).GetField<Messages.Structure[]>("structure");
        }

        public Messages.Structure GetStructure( int id )
        {
            return JsonObject.Create<Messages.Structure>( netman.MakeApiRequest( System.String.Format( "/api/v1.0/universe/structure/{0}", id ) ) );
        }
    }
}
