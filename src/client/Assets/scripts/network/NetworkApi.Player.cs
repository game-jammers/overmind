//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{

    public partial class NetworkApi
    {
        public Messages.Player[] GetPlayers()
        {
            return JsonObject.Create<Messages.Player>(netman.MakeApiRequest( "/api/v1.0/player" ), "players");
        }
    }
}
