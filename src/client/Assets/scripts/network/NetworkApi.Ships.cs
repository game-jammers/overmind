//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{

    public partial class NetworkApi
    {
        public Messages.Ship[] GetShips()
        {
            return JsonObject.Create<Messages.Ship>(netman.MakeApiRequest( "/api/v1.0/ship" ), "ships");
        }

        public Messages.Ship[] GetShips( ShipClass shipClass )
        {
            return JsonObject.Create<Messages.Ship>(netman.MakeApiRequest(
                System.String.Format("/api/v1.0/ship?class={0}", shipClass.ToString())), "ships"
            );
        }
    }
}
