//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    //
    // request #################################################################
    //

    [JsonAction("debug")]
    public class DebugPacket
        : JsonAction
    {
        public string message;

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["msg"] = message;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            message = json.GetField<string>("msg");
        }
    }
}
