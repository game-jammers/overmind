//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    //
    // request #################################################################
    //

    [JsonAction("signin")]
    public class SignInRequest
        : JsonAction
    {
        public string username;
        public string password;

        public static SignInRequest Create( string _username, string _password )
        {
            SignInRequest result = new SignInRequest();
            result.username = _username;
            result.password = _password;
            return result;
        }

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["username"] = username;
            json["password"] = password;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            username = json.GetField<string>("username");
            password = json.GetField<string>("password");
        }
    }

    //
    // response ################################################################
    //

    [JsonAction("signin_res")]
    public class SignInResponse
        : JsonAction
    {
        public bool success;
        public User user;

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["success"] = success;
            json["user"] = user;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            success = json.GetField<bool>("success");
            user = json.GetField<User>("user");
        }
    }
}
