//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    //
    // request #################################################################
    //

    [JsonAction("register")]
    public class RegisterRequest
        : JsonAction
    {
        public string username;
        public string password;

        public static RegisterRequest Create( string _username, string _password )
        {
            RegisterRequest result = new RegisterRequest();
            result.username = _username;
            result.password = _password;
            return result;
        }

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["username"] = username;
            json["password"] = password;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            username = json.GetField<string>("username");
            password = json.GetField<string>("password");
        }
    }

    //
    // response ################################################################
    //

    [JsonAction("register_res")]
    public class RegisterResponse
        : JsonAction
    {
        public bool success;

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["success"] = success;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            success = json.GetField<bool>("success");
        }
    }
}
