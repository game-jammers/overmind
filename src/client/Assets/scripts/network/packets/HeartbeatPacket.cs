//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    //
    // request #################################################################
    //

    [JsonAction("heartbeat")]
    public class HeartbeatPacket
        : JsonAction
    {
        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
        }
    }
}
