//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using blacktriangles.Network;

namespace GameJammers.Overmind
{
    //
    // request #################################################################
    //

    [JsonAction("selectship")]
    public class SelectShipRequest
        : JsonAction
    {
        public string selection;

        public static SelectShipRequest Create( string _selection )
        {
            SelectShipRequest result = new SelectShipRequest();
            result.selection = _selection;
            return result;
        }

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["selection"] = selection;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            selection = json.GetField<string>("selection");
        }
    }

    //
    // response ################################################################
    //

    [JsonAction("selectship_res")]
    public class SelectShipResponse
        : JsonAction
    {
        public bool success;
        public User user;

        public override JsonObject ToJson()
        {
            JsonObject json = base.ToJson();
            json["success"] = success;
            json["user"] = user;
            return json;
        }

        public override void FromJson( JsonObject json )
        {
            base.FromJson(json);
            success = json.GetField<bool>("success");
            user = json.GetField<User>("user");
        }
    }
}
