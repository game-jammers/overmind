//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================


using UnityEngine;
using System.Collections;

namespace GameJammers.Overmind
{
    public class Billboard
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////////
        public Camera cam;

        // unity callbacks /////////////////////////////////////////////////////////
        protected virtual void Update()
        {
            if( cam == null )
            {
                cam = SceneController.instance.cam.unityCamera;
            }

            transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward,
                cam.transform.rotation * Vector3.up);
        }
    }
}
