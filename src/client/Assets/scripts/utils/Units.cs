//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.Overmind
{
    public enum UnitType
    {
        Meter,
        Kilometer,
        Megameter,
        AstronomicalUnit,
        Lightyear,
        Parsec
    };

    public static class Units
    {
        // constants ///////////////////////////////////////////////////////////
        public const float kGlobalScale                     = 0.1f;
        public static readonly float[] kConversions = new float[] {
            1.0f,
            1000.0f,
            1000000.0f,
            149600000000.0f,
            9461000000000000.0f,
            30860000000000000.0f,
        };

        // public methods //////////////////////////////////////////////////////
        public static float Convert( float value, UnitType from, UnitType to )
        {
            float mult = GetFactor( to ) / GetFactor( from );
            return value * mult * kGlobalScale;
        }

        public static float GetFactor( UnitType unit )
        {
            return kConversions[ (int)unit ];
        }
    }
}
