//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.Overmind
{
    public class Distance
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public float value;
        public UnitType unit                                { get; private set; }

        // constructor / destructor ////////////////////////////////////////////
        public Distance()
            : this( 0.0f )
        {
        }

        public Distance( float _value )
            : this( _value, UnitType.Meter )
        {
        }

        public Distance( Distance copy )
            : this( copy.value, copy.unit )
        {
        }

        public Distance( float _value, UnitType _unit )
        {
            value = _value;
            unit = _unit;
        }

        // public methods //////////////////////////////////////////////////////
        public void Convert( UnitType to )
        {
            value = Units.Convert( value, unit, to );
            unit = to;
        }

        public Vector3 ToOrbit( Quaternion rotation )
        {
            float dist = Units.Convert( value, unit, UnitType.Meter );
            return rotation * Vector3.forward * dist;
        }

        public Distance Cast( UnitType to )
        {
            Distance result = new Distance( this );
            result.Convert( to );
            return result;
        }

        // operators ///////////////////////////////////////////////////////////
        public static Distance operator*( Distance dist, float mult )
        {
            dist.value *= mult;
            return dist;
        }

        public static Distance operator/( Distance dist, float div )
        {
            dist.value /= div;
            return dist;
        }

        public static Distance operator+( Distance lhs, Distance rhs )
        {
            Distance result = new Distance( lhs );
            Distance commonUnit = new Distance( rhs );
            commonUnit.Convert( lhs.unit );
            result.value += commonUnit.value;
            return result;
        }

        public static Distance operator-( Distance lhs, Distance rhs )
        {
            Distance result = new Distance( lhs );
            Distance commonUnit = new Distance( rhs );
            commonUnit.Convert( lhs.unit );
            result.value -= commonUnit.value;
            return result;
        }

        // json ////////////////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["value"] = value;
            json["unit"] = unit;
            return json;
        }

        public void FromJson( JsonObject json )
        {
            value = json.GetField<float>("value");
            unit = json.GetField<UnitType>("unit");
        }
    }
}
