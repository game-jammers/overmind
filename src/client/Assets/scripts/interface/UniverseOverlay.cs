//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class UniverseOverlay
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public UniverseMode universe                             { get; set; }
        public Text systemName                                   = null;
        public Text distance                                     = null;
        public Text description                                  = null;
        public Text pathDisp                                     = null;

        // public methods //////////////////////////////////////////////////////
        public override void Refresh()
        {
            base.Refresh();
            if( universe == null ) return;

            bool show = false;

            if( universe.selectedSystem != null )
            {
                systemName.text = System.String.Format( "{0} ({1})", universe.selectedSystem.data.name, universe.selectedSystem.data.id );
                if( universe.activeSystem != null )
                {
                    show = true;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append( "Stars: " );
                    sb.Append( universe.selectedSystem.data.stars.Length );
                    sb.Append( "\n" );

                    sb.Append( "Planets: " );
                    sb.Append( universe.selectedSystem.data.planets.Length );
                    sb.Append( "\n" );

                    sb.Append( "Asteroid Fields: " );
                    sb.Append( universe.selectedSystem.data.asteroids.Length );

                    description.text = sb.ToString();
                }
            }

            if( show )
            {
                Show( 1.0f );
            }
            else
            {
                Hide( 0.0f );
            }
        }

        public void RefreshPath()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            float totalKpc = 0.0f;
            if( universe.path != null )
            {
                Vector3 lastPos = Vector3.zero;
                foreach( SolarSystemData data in universe.path )
                {
                    sb.Append( data.name );
                    sb.Append( " | " );
                    if( lastPos == Vector3.zero )
                    {
                        lastPos = SolarSystem.StellarToScenePos(data.location);
                    }
                    else
                    {
                        Vector3 curPos = SolarSystem.StellarToScenePos(data.location);
                        totalKpc += (lastPos - curPos).magnitude;
                        lastPos = curPos;
                    }
                }
            }
            pathDisp.text = sb.ToString();
            distance.text = System.String.Format( "{0:0.00} kpc (kiloparsec)", totalKpc );
        }
    }
}
