//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class ShipPanel
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public ShipData ship                                     { get { return _ship; } set { SetShip( value ); } }
        private ShipData _ship                                   = null;

        [SerializeField] Text nameText                           = null;
        [SerializeField] Text descriptionText                    = null;
        [SerializeField] StatItem hullStat                       = null;
        [SerializeField] StatItem dpsStat                        = null;
        [SerializeField] StatItem warpSpeed                      = null;
        [SerializeField] StatItem cargoSize                      = null;

        // public methods //////////////////////////////////////////////////////
        public override void Refresh()
        {
            base.Refresh();
            if( ship == null ) return;

            nameText.text = ship.data.name;
            descriptionText.text = ship.data.description;
            hullStat.nameText.text  = "Hull";       hullStat.valueText.text  = ship.data.hull.ToString();
            dpsStat.nameText.text   = "DPS";        dpsStat.valueText.text   = ship.data.dps.ToString();
            warpSpeed.nameText.text = "Warp Speed"; warpSpeed.valueText.text = System.String.Format( "{0} kps", ship.data.warpSpeed );
            cargoSize.nameText.text = "Cargo";      cargoSize.valueText.text = System.String.Format( "{0} m3", ship.data.cargo );
        }

        // private methods /////////////////////////////////////////////////////
        private void SetShip( ShipData newShip )
        {
            _ship = newShip;
            Refresh();
        }
    }
}
