//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class StatItem
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public Text nameText                                     = null;
        public Text valueText                                    = null;
    }
}
