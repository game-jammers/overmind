//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class LoadingOverlay
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public Text loadingText                                  = null;
        public float loading                                     = 0.0f;

        // public methods //////////////////////////////////////////////////////
        protected virtual void Update()
        {
            loadingText.text = System.String.Format( "Loading... {0}%", Mathf.Floor(loading * 100.0f) );
        }
    }
}
