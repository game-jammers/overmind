﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarAndPlanetNameGenerator : MonoBehaviour {

    public string[] twoLetters = { "am", "an", "at", "ba", "be", "bl", "ch", "ci", "cl", "cr", "de", "di", "dr", "eb", "el", "en", "er", "es", "ev", "ew", "ha", "ia", "in", "ip","is","it","iv","la","lu","ma","me","mo","na","nc","nd","ne","ni","nn","no","nu","on","or","ou","pt","rc","re","ri","rm","rr","ry","sc","se","st","te","ti","tt","ue","un","us","va","ve","wd","we","wo","wu","xa","xe","xi","xu","ya","ye","yi","yo","yu"};
    public string[] romanNumerals = { "I", "II", "III", "IV", "V", "VI", "VII", "VII", "VIII", "IX", "X" };

    public string generatedName;

	void Start ()
    {
        GenerateName();
	}
	
    	void GenerateName()
    {
        int numberOfLetterStrings = Random.Range(3, 5);

        string firstLetters     = twoLetters[Mathf.FloorToInt(Random.Range(0, 73))];
        string secondLetters    = twoLetters[Mathf.FloorToInt(Random.Range(0, 73))];
        string thirdLetters     = twoLetters[Mathf.FloorToInt(Random.Range(0, 73))];
        string fourthLetters    = "";
        string fifthLetters     = "";

        if (numberOfLetterStrings >= 4)
        {
            fourthLetters = twoLetters[Mathf.FloorToInt(Random.Range(0, 73))];
        }
        if (numberOfLetterStrings == 5)
        {
            fifthLetters = twoLetters[Mathf.FloorToInt(Random.Range(0, 73))];
        }

        string addNumeral       = romanNumerals[Mathf.FloorToInt(Random.Range(0, 9))];

        generatedName = firstLetters + secondLetters + thirdLetters + fourthLetters + fifthLetters + " " + addNumeral;

        Debug.Log(generatedName);

    }





	void Update ()
    {
		
	}
}
