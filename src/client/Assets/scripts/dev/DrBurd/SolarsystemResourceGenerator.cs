﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarsystemResourceGenerator : MonoBehaviour {

    public int oxygen;
    public int carbon;
    public int iron;
    public int nitrogen;
    public int silicon;
    public int magnesium;
    public int sulfur;

    public int oxygenLevel;
    public int carbonLevel;
    public int ironLevel;
    public int nitrogenLevel;
    public int siliconLevel;
    public int magnesiumLevel;
    public int sulfurLevel;


    void Start () {

        CreateSystemResources();
    }
	


    void CreateSystemResources()
    {
        oxygen      = Random.Range(1, 10) * 1000000;
        carbon      = Random.Range(44, 440) * 10000;
        iron        = Random.Range(11, 110) * 10000;
        nitrogen    = Random.Range(9, 90) * 10000;
        silicon     = Random.Range(6, 60) * 10000;
        magnesium   = Random.Range(55, 550) * 1000;
        sulfur      = Random.Range(4, 40) * 10000;

        oxygenLevel     = Mathf.FloorToInt(oxygen - 1899999) / 900000;
        carbonLevel     = Mathf.FloorToInt(carbon - 835999) / 396000;
        ironLevel       = Mathf.FloorToInt(iron - 208999) / 99000;
        nitrogenLevel   = Mathf.FloorToInt(nitrogen - 104499) / 49500;
        siliconLevel    = Mathf.FloorToInt(silicon - 113999) / 54000;
        magnesiumLevel  = Mathf.FloorToInt(magnesium - 104499) / 49500;
        sulfurLevel     = Mathf.FloorToInt(sulfur - 75999) / 36000;

        
    }

	
	void Update () {
		
	}
}
