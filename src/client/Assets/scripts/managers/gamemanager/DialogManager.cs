//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using blacktriangles;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public enum DialogType
    {
        Notice,
        Confirmation
    }

    public class DialogManager
        : MonoBehaviour
    {
        // types ///////////////////////////////////////////////////////////////
        private class Request
        {
            public DialogType type;
            public UIDialog.Request data;

            public Request( DialogType _type, UIDialog.Request _data  )
            {
                type = _type;
                data = _data;
            }
        }

        // members /////////////////////////////////////////////////////////////
        public UIDialog noticeDialog                        = null;
        public UIDialog confirmDialog                       = null;
        public LoadingOverlay loadingOverlay                = null;
        [SerializeField] List<Request> requestQueue         = new List<Request>();
        [SerializeField] Request activeRequest              = null;

        // public methods //////////////////////////////////////////////////////
        public void AddDialog( DialogType type, UIDialog.Request data )
        {
            Request request = new Request( type, data );
            lock( requestQueue )
            {
                requestQueue.SortedInsert( request, (req1,req2)=>{ return req1.data.priority >= req2.data.priority; } );
            }
        }

        public void Notice( string title, string description, System.Action<UIDialog.Result> cb = null, int priority = 0, string accept = "OK" )
        {
            UIDialog.Request request = new UIDialog.Request();
            request.priority = priority;
            request.title = title;
            request.description = description;
            request.accept = accept;
            request.fadeInTime = 1.0f;
            request.fadeOutTime = 1.0f;
            request.callback = cb;
            AddDialog( DialogType.Notice, request );
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            noticeDialog.Hide( 0.0f );
            confirmDialog.Hide( 0.0f );
            loadingOverlay.Hide( 0.0f );
        }

        protected virtual void Update()
        {
            if( activeRequest != null ) return;

            lock( requestQueue )
            {
                if( requestQueue.Count > 0 )
                {
                    activeRequest = requestQueue[0];
                    requestQueue.RemoveAt(0);
                }
            }

            if( activeRequest != null )
            {
                UIDialog dialog = GetDialogByType( activeRequest.type );
                dialog.OnClose += OnDialogComplete;
                dialog.Show( activeRequest.data );
            }
        }

        // private methods /////////////////////////////////////////////////////
        private UIDialog GetDialogByType( DialogType type )
        {
            switch( type )
            {
                case DialogType.Notice: return noticeDialog;
                case DialogType.Confirmation: return confirmDialog;
            }

            DebugUtility.Error( "Requested unknown dialog type: " + type.ToString() );
            return null;
        }

        private void OnDialogComplete( UIDialog dialog, UIDialog.Result result )
        {
            activeRequest = null;
        }
    }
}
