//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class GameManager
        : BaseGameManager
    {
        // constants //////////////////////////////////////////////////////////////
        public static readonly string kPrefabPath                = "managers/GameManager";

        // accessors //////////////////////////////////////////////////////////////
        public static new GameManager instance                   { get; private set; }

        public User user                                         = null;
        public NetworkManager network                            = null;
        public DialogManager dialog                              = null;

        // public methods /////////////////////////////////////////////////////////
        public static GameManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
                Database.Initialize();
            }

            return instance;
        }

        public void DelayCall( float time, System.Action action )
        {
            StartCoroutine( DelayCallInternal( time, action ) );
        }

        public void ChangeScene( string sceneName )
        {
            if( UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != sceneName )
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene( sceneName );
            }
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Update()
        {

        }

        // private methods ////////////////////////////////////////////////////////
        private IEnumerator DelayCallInternal( float time, System.Action action )
        {
            yield return new WaitForSeconds( time );
            action();
        }
    };
}
