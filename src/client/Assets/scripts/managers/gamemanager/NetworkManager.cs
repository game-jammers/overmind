//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Network;

using System.Collections;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class NetworkManager
        : MonoBehaviour
    {
        // constants ///////////////////////////////////////////////////////////
        private const string localHost                           = "localhost";
        private const string liveHost                            = "dev.blacktriangles.net";
        private const int restPort                               = 9997;
        private const int pushPort                               = 9996;
        private const int kMaxConnectionAttempts                 = 10;
        private const float kSendHeartbeatPeriod                 = 2.0f;

        // members /////////////////////////////////////////////////////////////
        private Connection connection                              = new Connection();
        private PacketRouter packetRouter                        = new PacketRouter();
        private ActionRouter actionRouter                        = new ActionRouter();

        public bool isConnected                                  { get { return connection.isConnected; } }
        public ActionRouter router                               { get { return actionRouter; } }
        public NetworkApi api                                    { get; private set; }

        private string activeHost;

        private NetworkError err                                 = null;

        [SerializeField] private bool connectToLocalServer       = false;

        // public methods //////////////////////////////////////////////////////
        public bool Connect()
        {
            if( activeHost.Length <= 0 ) return false;
            connection.Connect( activeHost, pushPort );
            StartCoroutine( UpdateSocket() );
            return true;
        }

        public JsonObject MakeApiRequest( string request )
        {
            JsonObject json = null;

            string fullReq = System.String.Format( "http://{0}:{1}{2}", activeHost, restPort, request );
            bool result = RestClient.Request( fullReq, ref json );

            if( !result )
            {
                json = null;
            }

            return json;
        }

        public bool Send( JsonAction action )
        {
            if( isConnected == false ) return false;
            connection.SendPacket( new JsonPacket(action.ToJson()) );
            return true;
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            #if UNITY_EDITOR
                activeHost = connectToLocalServer ? localHost : liveHost;
            #else
                if( connectToLocalServer ) { connectToLocalServer = false; } // squelch the warning
                activeHost = liveHost;
            #endif

            connection.OnDisconnect += OnDisconnectNotice;
            connection.OnNetworkError += OnNetworkErrorNotice;
            packetRouter.AddRoute<JsonPacket>( actionRouter.Route );
            actionRouter.AddRoute<DebugPacket>( OnDebugPacket );
            actionRouter.AddRoute<HeartbeatPacket>( OnHeartbeat );
            api = new NetworkApi( this );
        }

        protected virtual void FixedUpdate()
        {
            if( err != null )
            {
                HandleError( err );
                err = null;
            }
        }

        // private methods /////////////////////////////////////////////////////
        private IEnumerator UpdateSocket()
        {
            //
            // wait until we are connected or until we time out
            //
            int attempted = 0;
            while( connection.isConnected == false && ++attempted < kMaxConnectionAttempts )
            {
                yield return new WaitForSeconds( 0.0f );
            }

            float dt = 0;

            //
            // while we are connected
            //
            while( connection.isConnected )
            {
                dt += Time.deltaTime;

                //
                // process packets
                //
                List<RawPacket> packets = null;
                while( packets == null || packets.Count <= 0 )
                {
                    yield return new WaitForSeconds( 0.1f );
                    if( !connection.isConnected ) break;
                    packets = connection.TakePackets();
                }

                foreach( RawPacket packet in packets )
                {
                    if( packetRouter.Route( packet ) ) continue;
                }

                //
                // send heartbeat
                //
                if( dt >= kSendHeartbeatPeriod )
                {
                    dt = 0;
                    Send( new HeartbeatPacket() );
                }

                yield return new WaitForSeconds(0.1f);
            }
        }

        // network callbacks ///////////////////////////////////////////////////
        private void OnDisconnectNotice( BaseConnection connection )
        {
            err = new NetworkError( connection, "You have been disconnected from the server." );
        }

        private void OnNetworkErrorNotice( NetworkError error )
        {
            err = error;
        }

        private void HandleError( NetworkError error )
        {
            GameManager.instance.dialog.Notice(
                    "Lost Connection to the Server...",
                    error.message,
                    (UIDialog.Result res)=>{  Connect(); }
                );
            GameManager.instance.ChangeScene( "main_menu" );
        }

        private void OnDebugPacket( BaseConnection conn, DebugPacket packet )
        {
            DebugUtility.Log( "<Server Message>: " + packet.message );
        }

        private void OnHeartbeat( BaseConnection conn, HeartbeatPacket packet )
        {
            // noop
        }
    }
}
