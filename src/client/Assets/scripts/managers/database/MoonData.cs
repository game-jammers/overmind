//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class MoonData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public PlanetData planet                                 { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public MoonData( PlanetData _planet, int _id )
             : base( _id )
        {
            planet = _planet;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.Moon msg = GameManager.instance.network.api.GetMoon(id);
            Refresh( msg );
        }

        public void Refresh( Messages.Moon msg )
        {
            base.Refresh(msg);
        }
    }
}
