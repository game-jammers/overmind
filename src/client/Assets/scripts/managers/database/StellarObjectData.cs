//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public int id                                            { get; protected set; }
        public string name                                       { get; protected set; }
        public StellarLocation location                          { get; protected set; }

        // constructor / initializer ///////////////////////////////////////////
        protected StellarObjectData( int _id )
        {
            id = _id;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh( Messages.StellarObject msg )
        {
            id = msg.id;
            name = msg.name;
            location = msg.location;
        }
    }
}
