//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using GameJammers.Overmind;

namespace GameJammers.Overmind
{
    public class StellarLocation
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public Distance x                                        { get; private set; }
        public Distance y                                        { get; private set; }
        public Distance z                                        { get; private set; }


        // json ////////////////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject result = new JsonObject();
            result["x"] = x;
            result["y"] = y;
            result["z"] = z;
            return result;
        }

        public void FromJson( JsonObject json )
        {
            x = json.GetField<Distance>("x");
            y = json.GetField<Distance>("y");
            z = json.GetField<Distance>("z");
        }

        // converters //////////////////////////////////////////////////////////
        public Vector3 ToVector3( UnitType unit = UnitType.Lightyear )
        {
            return new Vector3(
                x.Cast( unit ).value,
                y.Cast( unit ).value,
                z.Cast( unit ).value
            );
        }
    }
}
