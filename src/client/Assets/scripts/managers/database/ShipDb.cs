//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace GameJammers.Overmind
{
    public class ShipDb
    {
        // members /////////////////////////////////////////////////////////////
        public ShipData[] ships                                      { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public ShipDb()
        {
            Messages.Ship[] shipData = GameManager.instance.network.api.GetShips();
            ships = new ShipData[ shipData.Length ];
            for( int i = 0; i < shipData.Length; ++i )
            {
                Messages.Ship data = shipData[i];
                ShipData ship = new ShipData();
                ship.data = data;
                string path = System.String.Format( "ships/{0}", ship.data.id );
                ship.prefab = Resources.Load<Ship>( path );
                DebugUtility.Assert( ship.prefab != null, "Could not locate prefab for " + ship.data.id + ", searched for at: " + path );
                ships[i] = ship;
            }
        }

        // public methods //////////////////////////////////////////////////////
        public ShipData[] FindShips( ShipClass type )
        {
            return FindShips( (ShipData s)=>{ return s.data.shipClass == type; } );
        }

        public ShipData FindShip( System.Predicate<ShipData> pred )
        {
            return ships.Find( pred );
        }

        public ShipData[] FindShips( System.Predicate<ShipData> pred )
        {
            return ships.FindAll( pred );
        }
    }
}
