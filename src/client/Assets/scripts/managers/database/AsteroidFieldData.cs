//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class AsteroidFieldData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public SolarSystemData system                            { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public AsteroidFieldData( SolarSystemData _system, int _id )
             : base( _id )
        {
            system = _system;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.AsteroidField msg = GameManager.instance.network.api.GetAsteroidField(id);
            Refresh( msg );
        }

        public void Refresh( Messages.AsteroidField msg )
        {
            base.Refresh(msg);
        }
    }
}
