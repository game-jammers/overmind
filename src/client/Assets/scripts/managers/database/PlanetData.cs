//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class PlanetData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public SolarSystemData system                            { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public PlanetData( SolarSystemData _system, int _id )
             : base( _id )
        {
            system = _system;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.Planet msg = GameManager.instance.network.api.GetPlanet(id);
            Refresh( msg );
        }

        public void Refresh( Messages.Planet msg )
        {
            base.Refresh(msg);
        }
    }
}
