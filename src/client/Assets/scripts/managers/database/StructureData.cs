//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class StructureData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public SolarSystemData system                            { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public StructureData( SolarSystemData _system, int _id )
             : base( _id )
        {
            system = _system;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.Structure msg = GameManager.instance.network.api.GetStructure(id);
            Refresh( msg );
        }

        public void Refresh( Messages.Structure msg )
        {
            base.Refresh(msg);
        }
    }
}
