//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace GameJammers.Overmind
{
    public class UniverseDb
    {
        //
        // members #############################################################
        //

        public SolarSystemData[] systems                         { get; private set; }

        public float loading                                     { get; private set; }

        // loading /////////////////////////////////////////////////////////////
        private Thread workerThread                              = null;
        private bool workFinished                                = true;

        //
        // constructor / initializer ###########################################
        //
        public UniverseDb()
        {
            loading = 0.0f;
        }


        //
        // public methods ######################################################
        //

        public void Load( System.Action<bool> callback )
        {
            PerformWork( LoadingThread, callback );
        }

        //
        // #####################################################################
        //

        public SolarSystemData FindSystem( int systemId )
        {
            return FindSystem( (SolarSystemData data)=>{
                return data.id == systemId;
            });
        }

        //
        // generic find methods ################################################
        //

        public SolarSystemData FindSystem( System.Predicate<SolarSystemData> predicate )
        {
            foreach( SolarSystemData data in systems )
            {
                if(predicate(data))
                {
                    return data;
                }
            }

            Debug.LogError("Failed to find System!");
            return null;
        }

        //
        // #####################################################################
        //

        public List<SolarSystemData> FindSystems( System.Predicate<SolarSystemData> predicate )
        {
            List<SolarSystemData> result = new List<SolarSystemData>();
            foreach( SolarSystemData data in systems )
            {
                if(predicate(data)) result.Add(data);
            }

            return result;
        }

        //
        // utilities ###########################################################
        //

        public void GetPath( SolarSystemData start, SolarSystemData end, System.Action<SolarSystemData[]> callback )
        {
            DebugUtility.Assert( callback != null, "Cannot get path with a null callback" );
            PerformWork(
                // worker
                ()=>{
                    FindPath( start, end, (SolarSystemData[] data)=>{
                        workFinished = true;
                        callback(data);
                    });
                },

                null
            );
        }

        //
        // private methods #####################################################
        //

        private void PerformWork( System.Action worker, System.Action<bool> callback )
        {
            if( workFinished && workerThread == null )
            {
                workFinished = false;
                GameManager.instance.StartCoroutine( WaitForWork(callback) );
                workerThread = new Thread( new ThreadStart( worker ) );
                workerThread.Start();

            }
            else
            {
                if( callback != null )
                {
                    callback( false );
                }
            }
        }

        //
        // #####################################################################
        //

        private void LoadingThread()
        {
            try
            {
                Messages.SolarSystem[] solarSystemMsgs = GameManager.instance.network.api.GetSolarSystems();

                systems = new SolarSystemData[ solarSystemMsgs.Length ];

                for( int i = 0; i < solarSystemMsgs.Length; ++i )
                {
                    Messages.SolarSystem msg = solarSystemMsgs[i];
                    systems[i] = new SolarSystemData(msg);

                    loading = (float)i / (float)solarSystemMsgs.Length;
                }

                for( int i = 0; i < systems.Length; ++i )
                {
                    Messages.SolarSystem data = solarSystemMsgs[i];
                    SolarSystemData system = systems[i];
                    for( int j = 0; j < data.connections.Length; ++j )
                    {
                        DebugUtility.Assert( system != null, "System is null!" );
                        SolarSystemData conn = FindSystem(data.connections[j]);
                        if( conn != null )
                        {
                            system.AddConnection( conn );
                        }
                    }
                }
            }
            catch( System.Exception ex )
            {
                DebugUtility.Error( "Loading Failed: " + ex.ToString() );
            }

            workFinished = true;
        }

        //
        // #####################################################################
        //

        private void FindPath( SolarSystemData start, SolarSystemData end, System.Action<SolarSystemData[]> callback )
        {
            if( start == null )
            {
                callback( null );
                return;
            }

            if( end == null )
            {
                callback( null );
                return;
            }

            if( start == end )
            {
                callback( new SolarSystemData[] { start } );
                return;
            }

            foreach( SolarSystemData node in systems )
            {
                if( node != null )
                {
                    node.fscore = Mathf.Infinity;
                    node.gscore = Mathf.Infinity;
                }
            }

            List<SolarSystemData> visited = new List<SolarSystemData>();
            Deque<SolarSystemData> stack = new Deque<SolarSystemData>();
            stack.AddToFront( start );
            Dictionary<int,SolarSystemData> cameFrom = new Dictionary<int,SolarSystemData>();

            start.gscore = 0;
            start.fscore = ( start.location.ToVector3() - end.location.ToVector3() ).magnitude;

            Deque<SolarSystemData> finalPath = new Deque<SolarSystemData>();
            bool foundPath = false;

            int depth = 0;
            while( stack.Count > 0 && foundPath == false )
            {
                SolarSystemData current = stack.GetFront();
                foreach( SolarSystemData node in stack )
                {
                    if( node.fscore < current.fscore )
                    {
                        current = node;
                    }
                }

                if( current.id == end.id )
                {
                    finalPath.AddToFront( current );
                    while( cameFrom.ContainsKey( current.id ) )
                    {
                        current = cameFrom[ current.id ];
                        finalPath.AddToFront( current );
                    }
                    foundPath = true;
                }
                else
                {
                    ++depth;
                    stack.Remove( current );
                    visited.Add( current );
                    foreach( SolarSystemData neighbor in current.connections )
                    {
                        if( neighbor == null )
                            continue;

                        if( visited.Contains( neighbor ) )
                            continue;

                        float tentative_gscore = current.gscore + ( current.location.ToVector3() - neighbor.location.ToVector3() ).magnitude;
                        if( stack.Contains( neighbor ) == false )
                        {
                            stack.AddToFront( neighbor );
                        }
                        else if( tentative_gscore > neighbor.gscore )
                        {
                            continue;
                        }
                        else if( depth > 25 )
                        {
                            DebugUtility.Warning( "Path find went deeper than 25 iterations?" );
                            continue;
                        }

                        cameFrom[ neighbor.id ] = current;
                        neighbor.gscore = tentative_gscore;
                        neighbor.fscore = neighbor.gscore + depth + ( neighbor.location.ToVector3() - end.location.ToVector3() ).magnitude;
                    }
                }
            }

            if( foundPath == false )
            {
                callback( null );
                return;
            }

            SolarSystemData[] result = new SolarSystemData[ finalPath.Count ];
            for( int i = 0; i < finalPath.Count; ++i )
            {
                result[i] = finalPath[i];
            }

            callback( result );
        }

        //
        // #####################################################################
        //

        private IEnumerator WaitForWork( System.Action<bool> callback )
        {
            while( !workFinished )
                yield return new WaitForSeconds( 0.1f );

            if( callback != null )
            {
                callback( true );
            }

            if( workerThread != null )
			{
				workerThread.Join( 3000 );
				workerThread = null;
			}
        }
    }
}
