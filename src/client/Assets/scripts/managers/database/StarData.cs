//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class StarData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public SolarSystemData system                            { get; private set; }

        // constructor / initializer ///////////////////////////////////////////
        public StarData( SolarSystemData _system, int _id )
             : base( _id )
        {
            system = _system;
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.Star msg = GameManager.instance.network.api.GetStar(id);
            Refresh( msg );
        }

        public void Refresh( Messages.Star msg )
        {
            base.Refresh(msg);
        }
    }
}
