//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
    public class SolarSystemData
        : StellarObjectData
    {
        // members /////////////////////////////////////////////////////////////
        public StarData[] stars                                  { get; private set; }
        public PlanetData[] planets                              { get; private set; }
        public AsteroidFieldData[] asteroids                     { get; private set; }
        public StructureData[] structures                        { get; private set; }
        public List<SolarSystemData> connections                 { get; private set; }

        public float fscore                                      = Mathf.Infinity;
        public float gscore                                      = Mathf.Infinity;

        // constructor / initializer ///////////////////////////////////////////
        public SolarSystemData( Messages.SolarSystem msg )
            : base( msg.id )
        {
            Refresh( msg );
        }

        public void AddConnection( SolarSystemData connection )
        {
            if( connections == null ) connections = new List<SolarSystemData>();

            if( connection != null && connections.Contains( connection ) == false )
            {
                connections.Add( connection );
            }
        }

        // public methods //////////////////////////////////////////////////////
        public void Refresh()
        {
            Messages.SolarSystem msg = GameManager.instance.network.api.GetSolarSystem( id );
            Refresh( msg );
        }

        public void Refresh( Messages.SolarSystem msg )
        {
            base.Refresh( msg );

            stars = new StarData[ msg.children.stars.Length ];
            planets = new PlanetData[ msg.children.planets.Length ];
            asteroids = new AsteroidFieldData[ msg.children.asteroids.Length ];
            structures = new StructureData[ msg.children.structures.Length ];

            for( int i = 0; i < stars.Length; ++i )
            {
                stars[i] = new StarData( this, msg.children.stars[i] );
                stars[i].Refresh();
            }

            for( int i = 0; i < planets.Length; ++i )
            {
                planets[i] = new PlanetData( this, msg.children.planets[i] );
                planets[i].Refresh();
            }

            for( int i = 0; i < asteroids.Length; ++i )
            {
                asteroids[i] = new AsteroidFieldData( this, msg.children.asteroids[i] );
                asteroids[i].Refresh();
            }

            for( int i = 0; i < structures.Length; ++i )
            {
                structures[i] = new StructureData( this, msg.children.structures[i] );
                structures[i].Refresh();
            }
        }
    }
}
