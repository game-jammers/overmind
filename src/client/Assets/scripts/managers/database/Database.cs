//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections.Generic;

namespace GameJammers.Overmind
{
	public static class Database
	{
	    // members /////////////////////////////////////////////////////////////
        public static UniverseDb universe                        { get; private set; }
        public static ShipDb ship                                { get; private set; }

	    // constructor / destructor ////////////////////////////////////////////
	    static Database()
	    {
            Initialize();
	    }

		public static void Initialize()
		{
            universe = new UniverseDb();
            ship = new ShipDb();
		}
  }
}
