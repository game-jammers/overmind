//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public class ShipData
    {
        // members /////////////////////////////////////////////////////////////
        public Messages.Ship data                                = null;
        public Ship prefab                                       = null;
    }
}
