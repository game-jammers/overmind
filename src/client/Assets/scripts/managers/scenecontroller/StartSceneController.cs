//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Network;
using System.Collections;

namespace GameJammers.Overmind
{
    public class StartSceneController
        : SceneController
    {
        // members /////////////////////////////////////////////////////////////
        public NetworkManager network                            { get { return GameManager.instance.network; } }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            GameManager.instance.ChangeScene( "main_menu" );
        }
    }
}
