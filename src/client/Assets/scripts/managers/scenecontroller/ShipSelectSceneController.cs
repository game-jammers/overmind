//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Network;
using System.Collections;

namespace GameJammers.Overmind
{
    public class ShipSelectSceneController
        : SceneController
    {
        // members /////////////////////////////////////////////////////////////
        public NetworkManager network                            { get { return GameManager.instance.network; } }
        [SerializeField] ShipPanel shipPanel                     = null;
        [SerializeField] Transform shipHardpoint                 = null;

        private ShipData[] capitalShips                          = null;
        private int shipIndex                                    = 0;
        private Ship doll                                        = null;

        // ui callbacks ////////////////////////////////////////////////////////
        public void NextShip()
        {
            ++shipIndex;
            if( capitalShips.IsValidIndex( shipIndex ) == false )
            {
                shipIndex = 0;
            }
            SelectShip( capitalShips[shipIndex] );
        }

        public void PrevShip()
        {
            --shipIndex;
            if( capitalShips.IsValidIndex( shipIndex ) == false )
            {
                shipIndex = capitalShips.Length-1;
            }
            SelectShip( capitalShips[shipIndex] );
        }


        public void Launch()
        {
            SelectShipRequest req = SelectShipRequest.Create( capitalShips[shipIndex].data.id );
            network.Send( req );
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            capitalShips = Database.ship.FindShips( ShipClass.Capital );
        }

        protected virtual void Start()
        {
            SelectShip( capitalShips[shipIndex] );

            network.router.AddRoute<SelectShipResponse>( OnShipSelected );
        }

        protected virtual void Destroy()
        {
            network.router.RemoveRoute<SelectShipResponse>( OnShipSelected );
        }

        // private methods /////////////////////////////////////////////////////
        private void SelectShip( ShipData data )
        {
            if( doll != null )
            {
                Destroy(doll.gameObject);
            }

            doll = Instantiate( data.prefab );
            doll.transform.SetParent( shipHardpoint, false );
            shipPanel.ship = data;
        }

        private void OnShipSelected( BaseConnection conn, SelectShipResponse res )
        {
            if( res.success )
            {
                GameManager.instance.user = res.user;
                GameManager.instance.ChangeScene( "in_game" );
            }
            else
            {
                GameManager.instance.dialog.Notice( "Unable to select ship", "This should never appear, if it does send a scathing email to hsmith@jamming.games and tell him that you couldn't select a ship for some reason." );
            }
        }
    }
}
