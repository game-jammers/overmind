//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using System.Collections;
using System.Threading;

namespace GameJammers.Overmind
{
    public class InGameSceneController
        : SceneController
    {
        // members /////////////////////////////////////////////////////////////
        public NetworkManager network                            { get { return GameManager.instance.network; } }
        public GameMode activeMode                               { get; private set; }
        public UniverseMode universeMode                         { get { return _universeMode; } }

        private GameMode nextMode                                = null;
        [SerializeField] private UniverseMode _universeMode;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            if( GameManager.instance.user == null )
            {
                GameManager.instance.dialog.Notice( "Unexpected error", "Please relogin, notify hsmith@jamming.games if this problem persists" );
                GameManager.instance.ChangeScene( "main_menu" );
                return;
            }

            _universeMode.Initialize(this);
            Activate( universeMode );
        }

        protected virtual void Update()
        {
            if( activeMode != null )
            {
                activeMode.Update();
                if( activeMode.state == GameMode.State.Inactive )
                {
                    activeMode = null;
                }
            }

            if( nextMode != null )
            {
                if( activeMode == null || activeMode.state == GameMode.State.Inactive )
                {
                    Activate( nextMode );
                    nextMode = null;
                }
                else if( activeMode.state == GameMode.State.Active )
                {
                    Deactivate( activeMode );
                }
            }
        }

        protected virtual void Destroy()
        {
            Deactivate( activeMode );
        }

        // Will be called after all regular rendering is done
        protected void OnRenderObject()
        {
            if( activeMode == null ) return;

            GL.PushMatrix();
            //GL.MultMatrix(transform.localToWorldMatrix);
            activeMode.Render();
            GL.PopMatrix();
        }

        // private methods /////////////////////////////////////////////////////
        private void Activate( GameMode mode )
        {
            activeMode = mode;
            activeMode.Activate();
        }

        private void Deactivate( GameMode mode )
        {
            activeMode.Deactivate();
        }
    }
}
