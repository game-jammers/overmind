//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Network;
using System.Collections;

namespace GameJammers.Overmind
{
    public class MainMenuSceneController
        : SceneController
    {
        // members /////////////////////////////////////////////////////////////
        public NetworkManager network                            { get { return GameManager.instance.network; } }

        [SerializeField] private Text statusText                 = null;
        [SerializeField] private InputField username             = null;
        [SerializeField] private InputField password             = null;
        [SerializeField] private UIElement connectedBanner       = null;

        private float lastUpdate                                 = -1.0f;

        // public methods //////////////////////////////////////////////////////
        public void SignIn()
        {
            SignIn( username.text, password.text );
        }

        public void Register()
        {
            Register( username.text, password.text );
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            connectedBanner.Hide(0.0f);

            network.router.AddRoute<RegisterResponse>( OnRegister );
            network.router.AddRoute<SignInResponse>( OnSignIn );

            network.Connect();
        }

        protected virtual void Update()
        {
            if( lastUpdate <= 0.0f || Time.time > lastUpdate + 5.0f )
            {
                lastUpdate = Time.time;
                Messages.ServerStatus status = network.api.GetStatus();
                if( status != null && System.String.IsNullOrEmpty(status.apiVersion) == false )
                {
                    statusText.text = System.String.Format( "Server Status: <color=\"#00FF00\">ONLINE</color>\nVersion: {0} | Active Players: {1}"
                                                            , status.apiVersion, status.activePlayers );
                }
                else
                {
                    statusText.text = "Server Status: <color=\"#FF0000\">OFFLINE</color>";
                }
            }
        }

        protected virtual void Destroy()
        {
            network.router.RemoveRoute<RegisterResponse>( OnRegister );
            network.router.RemoveRoute<SignInResponse>( OnSignIn );
        }

        // private methods /////////////////////////////////////////////////////
        private void SignIn( string _username, string _password )
        {
            if( network.isConnected == false ) return;
            SignInRequest req = SignInRequest.Create( _username, _password );
            network.Send( req );
        }

        private void Register( string _username, string _password )
        {
            if( network.isConnected == false ) return;
            RegisterRequest req = RegisterRequest.Create( _username, _password );
            network.Send( req );
        }

        // network callbacks ///////////////////////////////////////////////////
        private void OnRegister( BaseConnection conn, RegisterResponse res )
        {
            if( res.success )
            {
                SignIn();
            }
            else
            {
                GameManager.instance.dialog.Notice( "Unable to register", "The username has already been taken." );
            }
        }

        private void OnSignIn( BaseConnection conn, SignInResponse res )
        {
            if( res.success )
            {
                GameManager.instance.user = res.user;
                if( GameManager.instance.user.shipId == null || GameManager.instance.user.shipId == "none" )
                {
                    GameManager.instance.ChangeScene( "ship_select" );
                }
                else
                {
                    GameManager.instance.ChangeScene( "in_game" );
                }
            }
            else
            {
                GameManager.instance.dialog.Notice( "Unable to sign in", "The username and password combination is invalid." );
            }
        }
    }
}
