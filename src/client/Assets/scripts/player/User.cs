//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Network;
using System.Collections;

namespace GameJammers.Overmind
{
    public class User
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public string id                                         { get; private set; }
        public string username                                   { get; private set; }
        public int location                                      { get; private set; }
        public string shipId                                     { get; private set; }

        // json ////////////////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["id"] = id;
            json["name"] = username;
            json["location"] = location;
            json["ship"] = shipId;
            return json;
        }

        public void FromJson( JsonObject json )
        {
            id = json.GetField<string>( "id" );
            username = json.GetField<string>( "name" );
            location = json.GetField<int>( "location" );
            shipId = json.GetField<string>( "ship" );
        }
    }
}
