//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace GameJammers.Overmind
{
    public enum ShipClass
    {
        Capital,
        Battleship,
        Cruiser,
        Destroyer,
        Corvette,
        Fighter,
        Probe
    };

    public class Ship
        : MonoBehaviour
    {
    }
}
